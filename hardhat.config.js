require('dotenv').config();
require("@nomiclabs/hardhat-ethers");
require("@nomicfoundation/hardhat-chai-matchers");
const { sepolia_url, private_key } = process.env;

module.exports = {
    solidity: "0.8.11",
    networks: {
        hardhat: {},
        sepolia: {
            url: sepolia_url,
            accounts: [`0x${private_key}`],
            gas: "auto", // Set gas to "auto" to use the default gas limit
            gasPrice: "auto", // Set gasPrice to "auto" to use the network default gas price
        }
    },
}
