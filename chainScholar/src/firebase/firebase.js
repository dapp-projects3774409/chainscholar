// firebase.js

import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
   
  };
  
  const app = firebase.initializeApp(firebaseConfig);
  
  export const auth = app.auth();
  export const googleProvider = new firebase.auth.GoogleAuthProvider();
  
  export const signInWithGoogle = async () => {
    try {
      await auth.signInWithPopup(googleProvider);
    } catch (error) {
      console.error(error);
    }
  };
  
  export default app;