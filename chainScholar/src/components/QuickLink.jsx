import React from "react";

function QuickLinks() {
  return (
    <div className="Quicklink">
      <h3>Quick-Links</h3>
      <ul>
        <li>
          <a href="#">GovTech Agency</a>
        </li>
        <li>
          <a href="#">Ministry of Education and Skills Development</a>
        </li>
        <li>
          <a href="#">Ministry of Industry Commerce & Employment</a>
        </li>
        <li>
          <a href="#">G2C Services</a>
        </li>
        <li>
          <a href="#">Royal Civil Service Commission</a>
        </li>
        <li>
          <a href="#">Royal University of Bhutan</a>
        </li>
      </ul>
    </div>
  );
}

export default QuickLinks;
