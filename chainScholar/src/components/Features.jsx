import React from 'react';
import logo from '../images/logo.png';

import picture from '../images/coinLogo.png';
import picture3 from '../images/picture3.png';
import picture2 from '../images/picture2.png';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import TwitterIcon from '@material-ui/icons/Twitter';

function Features(){
    return (
<>
        <div id='features'>
            <div className='a-container'>
                <img src={picture} alt=''/>
            </div>
            <h2 className="text-center">G-coin powering the ChainScholars ecosystem</h2> 
            <button className="login-button">Explore</button>
        </div>
            <div className = 'name row '>
                <div className='col-md-6 d-flex flex-column justify-content-center align-items-start'>
                    <img className="img-feature" src={picture3} alt = ''/>
                </div>
                <div className='col-md-6 d-flex flex-column justify-content-center'>
                    <h2><span>Decentralized, Incentivized, Transparent</span></h2>
                    <p>ChainScholars is a decentralized platform that incentivizes active participation through rewards, fostering collaboration and recognition, while ensuring transparency and immutability through blockchain technology.
                    </p>
                </div>

                <div className='mt-1 col-md-6 d-flex flex-column justify-content-center'>
                    <h2><span>Why Custom Reward Mechanism?</span></h2>
                    <p>
                    A custom reward mechanism within ChainScholars encourages engagement and contributions, ensuring users are recognized and incentivized for their valuable input and collaboration within the academic community.                    </p>
                </div>
                <div className='mt-1 col-md-6 d-flex flex-column justify-content-center align-items-end'>
                    <img className="img-feature" src={picture2} alt = ''/>
                </div>
                <div className='row footer-container p-4 d-flex flex-row justify-content-between'>
                    <div className='col-md-4 address-details'>
                        <h5>Address</h5>
                        <p>Gyalpozhing College of Information Technology</p>
                        <p>Royal University of Bhutan</p>
                        <p>Charnjekha, Thimphu, Bhutan</p>
                        <p>Contact No: +975 17618029</p>
                        <p>gciit@rub.edu.bt</p>
                    </div>
                    <div className='col-md-4 address-details'>
                        <h5>Policy</h5>
                        <p>Ecosystem Priciples</p>
                        <p>White paper</p>
                    </div>
                    <div className='col-md-4 address-details'>
                        <h5>Quick-links</h5>
                        <p>GovTech Agency</p>
                        <p>Ministry of Education and Skills Development</p>
                        <p>Ministry of Industry Commerce & Employment</p>
                        <p>G2C Services</p>
                        <p>Royal Civil Service Commission</p>
                        <p>Royal University of Bhutan</p>
                    </div>
                    <hr className='mt-1 pb-1'/>
                    <div className="row">
                        <div className="footer1 align-items-center  d-flex justify-content-between">
                            <img src={logo} alt = '' className="logo1 "/>                       
                            <div className="icon d-flex gap-3">
                                    <FacebookIcon />
                                    <InstagramIcon />
                                    <TwitterIcon />
                            </div>
                        </div>
                        <p className='text-center footer-text'>Copyright@ ChainScholars 2024. All Rights Reserved.</p>
                    </div>
                </div>
            </div>  

</>
    )
}

export default Features;