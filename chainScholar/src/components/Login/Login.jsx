import React from "react";
import "../../App.css";
import logo from "../../images/logo.png";
import { Link } from "react-router-dom";
import EmailOTPIcon from "@mui/icons-material/LockOpenOutlined";
import { useNavigate } from "react-router-dom";
import { useState } from "react";

const LOGIN = () => {
  // Define state   variables for form inputs
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  // Function to handle form submission
  const handleSubmit = (event) => {
    event.preventDefault();

    console.log("Email", email);
    console.log("Password", password);

    if (!password || !email) {
      alert("All fields are required");
    }
    fetch("https://chainscholarbackend.onrender.com/api/auth/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === "Success") {
          alert("Login successful!");
          setEmail("");
          setPassword("");
          navigate("/dashboard");
        } else {
          alert("Login failed. Please try again.");
        }
      });
  };
  return (
    <React.Fragment>
      <div className="registration-form-container ">
        <nav className="navbar">
          <a href="." className="logo">
            <img src={logo} alt="" />
          </a>
        </nav>
        <div className="registration-form">
          <h2 style={{ color: "#7E22CE" }}>Login</h2>
          <div className="icon-div pt-3">
            <div className="icon-container">
              <EmailOTPIcon className="email-verification mt-3" />{" "}
            </div>
          </div>
          <form className="input-form mt-3" onSubmit={handleSubmit}>
            <div className="form-group">
              <label>Email</label>
              <input
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label>Password</label>
              <input
                type="password"
                placeholder="Enter password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            {/* <Link> */}
            <button className="mt-2" type="submit">
              Login
            </button>
            {/* </Link> */}
          </form>
          <div className="form-group d-flex justify-content-end terms mt-2">
            <h6 className="condition">
              Do not have an account?{" "}
              <a className="text-decoration-none" href="/sign-up">
                Sign up
              </a>
            </h6>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default LOGIN;
