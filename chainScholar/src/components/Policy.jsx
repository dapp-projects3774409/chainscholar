import React from "react";

function Policy() {
  return (
    <div className="Policy">
      <h3>Policy</h3>
      <ul>
        <li>
          <a href="#">Ecosystem Priciples</a>
        </li>
        <li>
          <a href="#">White paper</a>
        </li>
      </ul>
    </div>
  );
}

export default Policy;
