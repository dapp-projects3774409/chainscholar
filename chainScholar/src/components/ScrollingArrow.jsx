import React, { useState, useEffect } from "react";
import "../styles/scroll.css";
import arrow from "../img/upArrow.png";

function ScrollToTopArrow() {
  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    const toggleVisibility = () => {
      if (window.pageYOffset > 500) {
        setIsVisible(true);
      } else {
        setIsVisible(false);
      }
    };

    window.addEventListener("scroll", toggleVisibility);

    return () => window.removeEventListener("scroll", toggleVisibility);
  }, []);

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <div className="scroll-to-top">
      {isVisible && (
        <button onClick={scrollToTop}>
          <span className="arrow-up">
            <img src={arrow} />
          </span>
        </button>
      )}
    </div>
  );
}

export default ScrollToTopArrow;
