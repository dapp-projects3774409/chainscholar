import "../styles/Signup.css";
import "../styles/LandingPage.css";
import logo from "../img/logo.png";
import staff from "../img/picture4.png";
import student from "../img/picture5.png";
import React, { useState } from "react";

function Signup() {
  const [accountType, setAccountType] = useState("");

  const handleAccountTypeClick = (type) => {
    setAccountType(type);
  };

  return (
    <div className="container">
      <header>
        <div className="logo">
          <img
            src={logo}
            alt="logo"
            style={{ width: "110px", height: "80px", marginLeft: "15px" }}
          />
        </div>
        <nav>
          <a href="#main-content">Home</a>
          <a href="#content2">About</a>
          <a href="#text-content">Mission</a>
          <button className="signup-button">
            <span>Login</span>
          </button>
        </nav>
      </header>
      <div className="signup-container">
        <h2 style={{ fontSize: "32px", marginTop: "-4px" }}>Signup</h2>
        <p style={{ fontSize: "20px" }}>Choose Account Type</p>
        <div className="account-type-options">
          <button
            className={`account-button ${
              accountType === "staff" ? "active" : ""
            }`}
            onClick={() => handleAccountTypeClick("staff")}
          >
            <img src={staff} alt="Staff" />
            Staff
          </button>
          <button
            className={`account-button ${
              accountType === "student" ? "active" : ""
            }`}
            onClick={() => handleAccountTypeClick("student")}
          >
            <img src={student} alt="Student" />
            Student
          </button>
        </div>

        {/* Form Inputs */}
        <input type="text" placeholder="Name" />
        <input type="email" placeholder="Email" />
        <input type="password" placeholder="Password" />
        <input type="password" placeholder="Confirm Password" />
        <div className="signupButton">
          <p>
            Have account?{" "}
            <a href="/login" style={{ color: "#7E22CE" }}>
              Login
            </a>
            <button type="submit">Sign up</button>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Signup;
