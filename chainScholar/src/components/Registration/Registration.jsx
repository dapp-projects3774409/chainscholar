import React, { useState } from "react";
import "../../App.css";
import pictureStaff from "../../images/picture4.png";
import student from "../../images/picture5.png";

import logo from "../../images/logo.png";
import { Link } from "react-router-dom";

const RegistrationForm = () => {
  const [step] = useState(1);

  const handleAccountType1 = () => {
    // Removing a specific item
    localStorage.removeItem("accountType");

    // Clearing the entire local storage
    localStorage.clear();

    // Storing data
    localStorage.setItem("accountType", "staff");
  };

  const handleAccountType2 = () => {
    // Removing a specific item
    localStorage.removeItem("accountType");
    // Storing data
    localStorage.setItem("accountType", "student");
  };
  return (
    <React.Fragment>
      <div className="registration-form-container ">
        <nav className="navbar">
          <a href="." className="logo">
            <img src={logo} alt="" />
          </a>
        </nav>
        <h2 className="form-title">Sign Up</h2>
        <div className="registration-form">
          <h2>Choose Account Type</h2>
          <div className="progress-dots">
            <span className={`dot ${step >= 1 ? "active" : ""}`}></span>
            <div
              className={`connector active-pro ${step >= 1 ? "active" : ""}`}
            ></div>
            <span className={`dot ${step >= 2 ? "active" : ""}`}></span>
            <div className={`connector ${step >= 2 ? "active" : ""}`}></div>
            <span className={`dot ${step >= 3 ? "active" : ""}`}></span>
          </div>
          <div className="social-login gap-3">
            <button
              className="google-btn d-flex flex-column"
              onClick={handleAccountType1}
            >
              <img src={pictureStaff} alt="Google Icon" />{" "}
              <span>
                <b>Staff</b>
              </span>
            </button>
            <button
              className="google-btn d-flex flex-column"
              onClick={handleAccountType2}
            >
              <img src={student} alt="Google Icon" />{" "}
              <span>
                <b>Student</b>
              </span>
            </button>
          </div>
          <form className="input-form">
            <Link to="/sign-up-details">
              <button type="submit">Next</button>
            </Link>
          </form>
          <div className="form-group d-flex justify-content-end terms mt-2">
            <h6 className="condition">
              Already have an account?{" "}
              <a className="text-decoration-none" href="/login">
                Login
              </a>
            </h6>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default RegistrationForm;
