import React, { useState } from "react";
import "../../App.css";
import logo from "../../images/logo.png";
import Offcanvas from "react-bootstrap/Offcanvas";
import EmailOTPIcon from "@mui/icons-material/AttachEmailOutlined";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import student from "../../images/picture5.png";

const CompanyDetails = () => {
  const [step] = useState(1);
  const [showOffcanvas, setShowOffcanvas] = useState(false);

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [otp, setOtp] = useState("");

  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    document.cookie = `email=${email}; expires=${new Date(
      Date.now() + 86400000
    ).toUTCString()}; path=/`;

    if (!name || !email || !password || !confirmPassword) {
      alert("Please fill in all fields.");
      return;
    }

    if (password !== confirmPassword) {
      alert("Passwords do not match.");
      return;
    }

    const accountType = localStorage.getItem("accountType");
    const photo = "default.jpg";

    fetch("https://chainscholarbackend.onrender.com/api/auth/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        accountType,
        name,
        email,
        photo,
        password,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data && data.status === "success") {
          alert("Registration successful!");
          setName("");
          setEmail("");
          setPassword("");
          setConfirmPassword("");
          localStorage.removeItem("accountType");
          setShowOffcanvas(true);
        } else {
          console.log(accountType, name, email, photo, password);
          alert("Registration failed. Please try again.");
        }
      })
      .catch((error) => console.error("Error:", error));

    console.log("Name:", name);
    console.log("Email:", email);
    console.log("Password:", password);
  };

  const handleCloseOffcanvas = () => {
    setShowOffcanvas(false);
  };

  function getCookie(name) {
    const cookieString = document.cookie;
    const cookies = cookieString.split("; ");
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i];
      const [cookieName, cookieValue] = cookie.split("=");
      if (cookieName === name) {
        return decodeURIComponent(cookieValue);
      }
    }
    return null;
  }

  const userEmail = getCookie("email");

  const verifyOtp = (e) => {
    e.preventDefault();

    fetch("https://chainscholarbackend.onrender.com/api/auth/verify", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: userEmail,
        otp: otp,
      }),
    })  
      .then((response) => response.json())
      .then((data) => {
        if (data.status === "Verified") {
          alert("OTP verification successful!");
          navigate("/Login");
        } else {
          alert("OTP verification failed. Please try again.");
        }
      })
      .catch((error) => console.error("Error:", error));

    console.log("Entered OTP:", otp);
  };

  return (
    <React.Fragment>
      <div className="registration-form-container">
        <nav className="navbar">
          <a href="." className="logo">
            <img src={logo} alt="" />
          </a>
          <input type="checkbox" className="menu-btn" id="menu-btn" />
          <label className="menu-icon" htmlFor="menu-btn">
            <span className="nav-icon"></span>
          </label>
        </nav>
        <h2 className="form-title">Sign Up</h2>
        <div className="registration-form">
          <h2>Provide Information</h2>
          <div className="progress-dots">
            <span className={`dot ${step >= 1 ? "active" : ""}`}></span>
            <div
              className={`connector active-pro1 ${step >= 1 ? "active" : ""}`}
            ></div>
            <span
              className={`dot dot-pro1 ${step >= 2 ? "active" : ""}`}
            ></span>
            <div
              className={`connector active-pro ${step >= 1 ? "active" : ""}`}
            ></div>
            <span className={`dot ${step >= 3 ? "active" : ""}`}></span>
          </div>
          <form className="input-form" onSubmit={handleSubmit}>
            <div className="form-group">
              <label>Name</label>
              <input
                type="text"
                placeholder="Enter name"
                onChange={(e) => setName(e.target.value)}
                value={name}
              />
            </div>
            <div className="form-group">
              <label>Email</label>
              <input
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label>Password</label>
              <input
                type="password"
                placeholder="Enter password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label>Confirm Password</label>
              <input
                type="password"
                placeholder="Confirm your password"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
            </div>
            <button className="mt-2" type="submit">
              Next
            </button>
          </form>
        </div>
      </div>

      <div>
        <Offcanvas
          show={showOffcanvas}
          onHide={handleCloseOffcanvas}
          placement="bottom"
          className="center-offcanvas"
        >
          <Offcanvas.Body className="body-offcanvas">
            <h5 className="text-center pt-3">Email verification</h5>
            <form className="p-3" onSubmit={verifyOtp}>
              <div className="icon-div">
                <div className="icon-container">
                  <EmailOTPIcon className="email-verification mt-3" />
                </div>
              </div>
              <div className="form-group mt-3 pb-2">
                <label>OTP</label>
                <input
                  type="text"
                  placeholder="Enter OTP"
                  name={otp}
                  onChange={(e) => setOtp(e.target.value)}
                />
              </div>
              <button type="submit">Submit</button>
            </form>
          </Offcanvas.Body>
        </Offcanvas>
      </div>
    </React.Fragment>
  );
};

export default CompanyDetails;
