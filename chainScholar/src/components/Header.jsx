import React from 'react';
import Navbar from './Navbar';
import picture from '../images/picture1.png';

function Header(){
    return (
        <div id='main'>
            <Navbar/>
            <div className = 'name names row mt-5 pt-5'>
                <div className='col-md-6 d-flex flex-column justify-content-end'>
                    <h1><span>ChainScholars</span></h1>
                    <p>Platform that rewards active participation and
                        fosters transparent knowledge exchange</p>
                    <a href='./Registration/Registration.jsx'>
                    <button class='login-button pt-1' >Learn More</button>
                    </a>
                </div>
                <div className='col-md-6 mt-1 d-flex flex-column justify-content- align-items-end'>
                    <img src={picture} alt = ''/>
                </div>
            </div>
        </div>
    )
}

export default Header;