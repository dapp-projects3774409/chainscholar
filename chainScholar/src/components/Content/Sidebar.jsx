import React, { useState } from 'react';
import "./sidebar.css"
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarHeader,
  CDBSidebarMenu,
} from 'cdbreact';
import { NavLink } from 'react-router-dom';
import logo from '../../images/logo.png';
import DashboardOutlinedIcon from '@mui/icons-material/DashboardOutlined';
import ManageSearchOutlinedIcon from '@mui/icons-material/ManageSearchOutlined';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import GroupsOutlinedIcon from '@mui/icons-material/GroupsOutlined';
import PermIdentityOutlinedIcon from '@mui/icons-material/PermIdentityOutlined';

const MySidebar = ({ setCurrentContent }) => {
  const [isSidebarOpen, setIsSidebarOpen] = useState(true);
  const [activeLink, setActiveLink] = useState('/dashboard'); // Initialize with the default active link

  const toggleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };

  const handleNavLinkClick = (path) => {
    setActiveLink(path);
  };

  return (
    <div className={`sidebar p-1 d-flex h-100vh ${isSidebarOpen ? 'sidebar-open' : 'sidebar-closed'}`}>
      <CDBSidebar backgroundColor='white' className="main-container">
        <CDBSidebarHeader  prefix={<i className="fa fa-bars fa-large" style={{color:'#7E22CE', fontSize:24}}  onClick={toggleSidebar}></i>}>
            <img src={logo} alt='' className='logo-sidebar'/>
        </CDBSidebarHeader>
        <CDBSidebarContent>
          <CDBSidebarMenu>
            <div className="content">
            <NavLink
                exact
                to="/staff-dashboard"
                className={`sidebar-link ${activeLink === '/staff-dashboard' && 'active-link'}`}
                onClick={() => handleNavLinkClick('/staff-dashboard')}
              >
                <i className="">
                  <DashboardOutlinedIcon />
                </i>
                {isSidebarOpen && (
                  <span className="ml-2">StaffDashboard</span>
                )}
              </NavLink>
              <NavLink
                exact
                to="/dashboard"
                className={`sidebar-link ${activeLink === '/dashboard' && 'active-link'}`}
                onClick={() => handleNavLinkClick('/dashboard')}
              >
                <i className="">
                  <DashboardOutlinedIcon />
                </i>
                {isSidebarOpen && (
                  <span className="ml-2">Dashboard</span>
                )}
              </NavLink>
              <NavLink
                exact
                to="/explore"
                className={`sidebar-link ${activeLink === '/explore' && 'active-link'}`}
                onClick={() => handleNavLinkClick('/explore')}
              >
                <i className="">
                  <ManageSearchOutlinedIcon />
                </i>
                {isSidebarOpen && (
                  <span className="ml-2">Explore</span>
                )}
              </NavLink>
              <NavLink
                exact
                to="/about"
                className={`sidebar-link ${activeLink === '/about' && 'active-link'}`}
                onClick={() => handleNavLinkClick('/about')}
              >
                <i className="">
                  <InfoOutlinedIcon />
                </i>
                {isSidebarOpen && (
                  <span className="ml-2">About</span>
                )}
              </NavLink>
              <NavLink
                exact
                to="/community"
                className={`sidebar-link ${activeLink === '/community' && 'active-link'}`}
                onClick={() => handleNavLinkClick('/community')}
              >
                <i className="">
                  <GroupsOutlinedIcon />
                </i>
                {isSidebarOpen && (
                  <span className="ml-2">Community</span>
                )}
              </NavLink>
              <NavLink
                exact
                to="/profile"
                className={`sidebar-link ${activeLink === '/profile' && 'active-link'}`}
                onClick={() => handleNavLinkClick('/profile')}
              >
                <i className="">
                  <PermIdentityOutlinedIcon />
                </i>
                {isSidebarOpen && (
                  <span className="ml-2">Profile</span>
                )}
              </NavLink>
            </div>
          </CDBSidebarMenu>
        </CDBSidebarContent>
      </CDBSidebar>
    </div>
  );
};

export default MySidebar;
