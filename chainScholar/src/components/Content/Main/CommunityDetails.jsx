import React, { useState } from 'react';
import img from '../../../images/img1.jpg';
import CheckOutlinedIcon from '@mui/icons-material/GroupOutlined';
import GroupAddOutlinedIcon from '@mui/icons-material/GroupAddOutlined';
import ChatBubbleOutlineOutlinedIcon from '@mui/icons-material/ChatBubbleOutlineOutlined';
function CommunityDetails() {
    const [joined, setJoined] = useState(false);

    const handleJoin = () => {
      setJoined(true);
    };
  return (
    <div className='row community'>
    <div className="justify-content-between mt-3 align-items-center">
      <div className="course-container p-4">
        <img className='image-course' src={img} alt='' />
        <div className="d-flex flex-row p-3 justify-content-between align-items-center">
          <div className="d-flex flex-column ">
            <h5>Programming for blockchain 1</h5>
            <span>Created by Tutor Lhamo</span>
            <span>10 <span>members</span></span>
          </div>
          <div>
            {joined ? (
                <button className="member-join d-flex gap-3" onClick={handleJoin}><CheckOutlinedIcon color="white" /> Joined</button>  
            ) : (
              <button className="member-join d-flex gap-3" onClick={handleJoin}><GroupAddOutlinedIcon color="white" /> Join</button>
            )}
          </div>
        </div>
        <div className="border rounded p-3 d-flex flex-column">
            <div className="d-flex flex-column">
                <span style={{color:"#7a7a7a", fontSize:12}}>5 <span>mins ago</span></span>
                <span> List and explain the parts of EVM memory</span>
            </div>
            <div className="d-flex justify-content-end">
                <button className="button-join d-flex gap-3" style={{background:'transparent', color:'#505050', fontSize:14}} onClick={handleJoin}><ChatBubbleOutlineOutlinedIcon color="white" /> replies</button>
            </div>
        </div>
      </div>
    </div>
  </div>
      
  )
}

export default CommunityDetails