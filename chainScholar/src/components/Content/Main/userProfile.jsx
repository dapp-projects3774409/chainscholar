import React from "react";
import CommunityList from "./userCommunity";
import EarningStatus from "./ExchangeStatus";
import logo from "../../../images/logo.png";
import { useState } from "react";
import "../styles/userProfile.css";
import student from "../../../images/picture5.png";
import upload from "../../../images/upload.png";

function Profile() {
  const [name, setName] = useState("name");
  const [email, setEmail] = useState("");
  const [file, setFile] = useState(null);

  // const handleNameChange = (e) => {
  //   setName(e.target.value);
  // };

  // const handleEmailChange = (e) => {
  //   setEmail(e.target.value);
  // };

  // const handleFileChange = (e) => {
  //   const file = e.target.files[0];
  //   setFile(file);
  // };

  const handleSubmit = () => {
    // Handle the renaming logic here
    console.log("New name:");
    // closeRenameModal();
  };

  return (
    <div className="profile-container">
      <div className="profileContent">
        <div className="profile-card">
          <img src={student} alt="Profile Picture" />
          <h2>John Lever</h2>
          <p>John@gmail.com</p>
          <p>Account: 0xasdfgh1fsdvd</p>
          {/* Add form for updating profile information */}
        </div>
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="name">Name:</label>
            <input
              type="text"
              id="name"
              value={name}
              onChange={(e) => setName(e.target.value)}
              required
            />
          </div>
          <div>
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              id="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </div>
          <div className="file-input-container">
            <label htmlFor="file" className="file-input-label">
              Picture:
              <img
                src={upload}
                alt="upload"
                style={{ width: "25px", height: "25px", marginRight: "85px" }}
              />
            </label>
            <input
              type="file"
              id="file"
              accept=".jpg, .jpeg, .png, .pdf"
              onChange={(e) => setFile(e.target.value)}
              required
            />
          </div>

          <button
            type="submit"
            className="updateButton"
            style={{
              backgroundColor: "#7e22ce",
              width: "85px",
              height: "32px",
              alignSelf: "center",
            }}
          >
            Update
          </button>
        </form>
      </div>

      <div className="info-section">
        <CommunityList />
        <EarningStatus />
        <img
          src={logo}
          alt="logo"
          style={{
            width: "230px",
            height: "170px",
            marginLeft: "60px",
          }}
        />
      </div>
      {/* popup exchang */}
      {/* <button onClick={toggleModal} className="btn-modal">
        Open
      </button> */}
    </div>
  );
}

export default Profile;
