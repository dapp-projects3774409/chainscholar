import React, { useState } from "react";
import downarrow from "../../../images/downArrow.png";
import "../styles/exchangeRecord.css";
import Modal from "react-modal";

function EarningStatus() {
  const [modal, setModal] = useState(false);
  const [gPoints, setGPoints] = useState("");
  const [gCoins, setGCoins] = useState("");

  const handleExchange = () => {
    // Perform exchange logic here
    // onClose();
    // Reset input fields
    setGPoints("");
    setGCoins("");
  };

  const toggleModal = () => {
    setModal(!modal);
  };

  if (modal) {
    document.body.classList.add("active-modal");
  } else {
    document.body.classList.remove("active-modal");
  }

  return (
    <div className="earning-status">
      <h3>Earning Status</h3>
      <p>R-points: 40</p>
      <p>G-coins: 0</p>
      <button
        onClick={toggleModal}
        style={{
          backgroundColor: "#7e22ce",
          width: "85px",
          height: "42px",
          padding: "8px",
          alignSelf: "center",
          border: "none",
          borderRadius: "5px",
          color: "white",
        }}
      >
        Exchange
      </button>
      {modal && (
        <div className="modal">
          <div onClick={toggleModal} className="overlay"></div>
          <div
            className="modal-content"
            style={{
              border: "1px solid black",
              background: "white",
              width: "310px",
              height: "240px",
              display: "flex",
              alignItems: "center",
              borderRadius: "5px",
            }}
          >
            <div className="exchange-content">
              <div className="coinLabels">
                <div
                  style={{
                    marginTop: "30px",
                    marginBottom: "10px",
                    marginLeft: "30px",
                  }}
                >
                  <label
                    htmlFor="G-points"
                    style={{
                      // background: "#7e22ce",
                      padding: "6px",
                      color: "Black",
                      borderRadius: "2px",
                      fontWeight: "bold",
                    }}
                  >
                    G-points
                  </label>
                  <input
                    type="number"
                    value={gPoints}
                    onChange={(e) => setGPoints(e.target.value)}
                  />
                </div>

                <img
                  src={downarrow}
                  alt="down Arrow"
                  style={{
                    background: "transparent",
                    width: "310px",
                    height: "32px",
                  }}
                />

                <div
                  style={{
                    marginTop: "10px",
                    marginRight: "20px",
                    marginLeft: "30px",
                  }}
                >
                  <label
                    htmlFor="G-coins"
                    style={{
                      // background: "#7e22ce",
                      padding: "6px",
                      color: "black",
                      borderRadius: "2px",
                      fontWeight: "bold",
                    }}
                  >
                    G-coins
                  </label>
                  <input
                    type="number"
                    readOnly
                    value={gCoins}
                    onChange={(e) => setGCoins(e.target.value)}
                  />
                </div>
              </div>

              <button
                style={{
                  backgroundColor: "#7e22ce",
                  color: "white",
                  border: "none",
                  padding: "8px 16px",
                  borderRadius: "4px",
                  cursor: "pointer",
                  marginLeft: "120px",
                }}
              >
                Exchange
              </button>
            </div>
            <button
              className="close-modal"
              onClick={toggleModal}
              style={{
                backgroundColor: "#7e22ce",
                color: "white",
                border: "none",
                fontSize: "10px",
                borderRadius: "4px",
                cursor: "pointer",
                marginLeft: "120px",
                marginTop: "-4px",
              }}
            >
              CLOSE
            </button>
          </div>
        </div>
      )}
    </div>
  );
}

export default EarningStatus;
