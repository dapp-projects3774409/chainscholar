import React from "react";
import logo from "../../../images/logo.png";

function Profile() {
  return (
    <div className="row">
      <div className="col-md-6">
        <div className="shadow p-5">
          <img className="profile-img13" src={logo} alt="" />
          <h5 className="text-center mt-4">Lhamo</h5>
          <span class="d-flex justify-content-center">lhamo123@gmail.com</span>
          <span class="d-flex justify-content-center">
            Account: <span>0xbA9664BE108E364E823fe4f4FdE960f82b84429D</span>
          </span>
        </div>
      </div>
      <div className="col-md-6 shadow">
        <div className=" p-3">
          <div className="form-group">
            <label>Full Name</label>
            <input type="text" />
          </div>
          <div className="form-group">
            <label>Email</label>
            <input type="text" />
          </div>
          <div className="form-group">
            <label>Picture</label>
            <input type="file" />
            <button className="member-join abc mt-4">Update</button>
          </div>
        </div>
      </div>
      <div className="col-md-6 mt-3">
        <div className="shadow p-5">
          <h5 className="text-center mt-2">
            Programming for <h5>blockchain1</h5>
          </h5>
          <span class="d-flex justify-content-center">
            Member No.<span> 123</span>
          </span>
          <span class="d-flex justify-content-center">
            BanMember No. <span> 03</span>
          </span>
          <span class="d-flex justify-content-center">
            G-coins <span>100</span>
          </span>
        </div>
      </div>
      <div className="col-md-6 shadow mt-3">
        <div className=" p-3">
          <div className="d-flex  gap-3 align-items-center">
            <span
              className="col-md-4"
              style={{ fontSize: 16, fontFamily: "medium" }}
            >
              Community Name
            </span>
            <span className="col-md-8">Programming for blockchain1</span>
          </div>
          <div className="d-flex gap-3 mt-4">
            <span
              className="col-md-4"
              style={{ fontSize: 16, fontFamily: "medium" }}
            >
              Community Name
            </span>
            <span className="col-md-8">
              A community dedicated to mastering the art of coding for
              decentralized echnologies.{" "}
            </span>
          </div>
          <button className="member-join abc mt-4">Update</button>
        </div>
      </div>
    </div>
  );
}

export default Profile;
