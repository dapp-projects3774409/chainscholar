import React from 'react'
import picture3 from '../../../images/picture1.png';

function About() {
  return (

    <div className = 'name row '>
        <div className='col-md-6 d-flex flex-column justify-content-center'>
            <h2><span>ChainScholars</span></h2>
            <p>Welcome to ChainScolars, where academic discourse meets blockchain innovation!</p>
        </div>
        <div className='col-md-6 d-flex flex-column justify-content-center align-items-start'>
            <img className="img-feature" src={picture3} alt = ''/>
        </div>
        <div className="col-sm-12">
            <h2><span>What is ChainScholars?</span></h2>
            <p>
            ChainScolars is a groundbreaking initiative that leverages blockchain technology to redefine the landscape of academic discourse. At ChainScolars, we understand the importance of transparent, immutable, and decentralized systems for fostering trust and reliability in knowledge sharing.
            </p>
        </div>
    </div>
)
}

export default About