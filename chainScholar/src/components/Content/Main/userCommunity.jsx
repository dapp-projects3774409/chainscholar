import React from "react";

function CommunityList() {
  const communities = [
    "Community Joined List",
    "Solidity CSC101",
    "Mlbb community",
    "Pubg community",
    "LIA community",
  ];

  return (
    <div className="community-list">
      <h3>Community Joined List</h3>
      <ul>
        {communities.map((community) => (
          <li key={community}>{community}</li>
        ))}
      </ul>
    </div>
  );
}

export default CommunityList;
