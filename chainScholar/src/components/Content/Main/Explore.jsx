
import React, { useState }  from 'react';
import { Link} from 'react-router-dom';


import 'bootstrap/dist/css/bootstrap.min.css';
import img1 from "../../../images/picture1.png"
import img2 from "../../../images/picture2.png"
import img3 from "../../../images/picture3.png"
import ChevronRightOutlinedIcon from '@mui/icons-material/ArrowForwardIosOutlined';
import ChevronLeftrtOutlinedIcon from '@mui/icons-material/ArrowBackIosNewOutlined';


const Explore = () => {
  const [activeIndex, setActiveIndex] = useState(0);

  // Function to handle button click and set active index
  const handleButtonClick = (index) => {
    setActiveIndex(index);
  };
  return (
    <div>
      <h5 className='mt-3'>Explore Communities</h5>
        <form className="input-form border p-4 rounded" >
            <div className="form-group row">
              <div className="col-md-9 mt-1">
                <input type="text" placeholder="Search community groups" />
              </div>
              <div className="col-md-3 mt-1">
                <button className="button-search">Search</button>
              </div>
              <div className="p-3 main-layout">
                <div id="carouselExampleCaptions" className="carousel slide border col-md-8">
                  <div className="carousel-indicators">
                    <button
                      onClick={() => handleButtonClick(0)}
                      type="button"
                      data-bs-target="#carouselExampleCaptions"
                      data-bs-slide-to="0"
                      className={`switch ${activeIndex === 0 ? 'active' : ''}`}
                      aria-current="true"
                      aria-label="Slide 1"
                    ></button>
                    <button
                      onClick={() => handleButtonClick(1)}
                      type="button"
                      data-bs-target="#carouselExampleCaptions"
                      data-bs-slide-to="1"
                      className={`switch ${activeIndex === 1 ? 'active' : ''}`}
                      aria-label="Slide 2"
                    ></button>
                    <button
                      onClick={() => handleButtonClick(2)}
                      type="button"
                      data-bs-target="#carouselExampleCaptions"
                      data-bs-slide-to="2"
                      className={`switch ${activeIndex === 2 ? 'active' : ''}`}
                      aria-label="Slide 3"
                    ></button>
                  </div>
                  <div className="carousel-inner">
                    <div className="carousel-item active">
                      <img src={img1} className="carousel-img" alt="..."/>
                      <div>
                      <div className="description carousel-caption">
                        <h5 className='text-center'>Programming for Blockchain</h5>
                        <p className="text-center">200 members</p>
                        <div className="join-container d-flex justify-content-end">
                        <Link to="/explore/community-details">
                          <button className="button-join">Join</button>
                        </Link>
                        </div>
                      </div>
                      </div>
                    </div>
                    <div className="carousel-item">
                      <img src={img2} className="carousel-img align-items-center" alt="..."/>
                      <div className="description carousel-caption">
                        <h5 className='text-center'>Second slide label</h5>
                        <p className="text-center">Some representative placeholder content for the second slide.</p>
                        <div className="join-container d-flex justify-content-end">
                        <Link to="/explore/community-details">
                          <button className="button-join">Join</button>
                          </Link>
                        </div>
                      </div>
                    </div>
                    <div className="carousel-item">
                      <img src={img3} className="carousel-img align-items-center" alt="..."/>
                      <div className="description carousel-caption">
                        <h5 className="text-center">Third slide label</h5>
                        <p className="text-center"> Some representative placeholder content for the third slide.</p>
                        <div className="join-container d-flex justify-content-end">
                        <Link to="/explore/community-details">
                          <button className="button-join">Join</button>
                        </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                    <span className="" ><i><ChevronLeftrtOutlinedIcon style={{fontSize:35}}/></i></span>
                    <span className="visually-hidden">Previous</span>
                  </button>
                  <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                    <span className="" aria-hidden="true"><i ><ChevronRightOutlinedIcon style={{fontSize:35}}/></i></span>
                    <span className="visually-hidden">Next</span>
                  </button>
                </div>
                <div className="col-md-4">
                  <div className="p-4 d-flex flex-column ">
                    <h5>Direction</h5>
                    <span>Put something that will guide the user about these community pages</span>
                    <span>Put something that will guide the user about these community pages</span>
                    <span>Put something that will guide the user about these community pages</span>
                  </div>
                </div>
              </div>
            </div>
      </form>
    </div>
  );
};

export default Explore;