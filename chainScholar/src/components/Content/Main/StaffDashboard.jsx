import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './main.css'
import AddOutlinedIcon from '@mui/icons-material/KeyboardDoubleArrowRightOutlined';
import logo from '../../../images/logo.png';
import ChatBubbleOutlineOutlinedIcon from '@mui/icons-material/ChatBubbleOutlineOutlined';
import ArrowCircleUpOutlinedIcon from '@mui/icons-material/ArrowCircleUpOutlined';
import Offcanvas from 'react-bootstrap/Offcanvas'; // Import Offcanvas component

function StaffDashboard() {
  const topUsers = ['Pema', 'Rada Dorji', 'Sapuna Mongar'];
  const [showOffcanvas, setShowOffcanvas] = useState(false); // State to manage the visibility of the offcanvas

  const handleCloseOffcanvas = () => {
    setShowOffcanvas(false);
  };

  const handleSubmit = () => {
    setShowOffcanvas(true);
  };
  return (
    <div className='row'>
      <div className="d-flex flex-row  mt-3 gap-2 align-items-center">
        <div className="card-body justify-content-end col-md-9 comment-layout1 d-flex align-items-center flex-row gap-2">
            <img className='profile-img1' src={logo} alt='' />
            <input type='text' className="border input-comment1" placeholder="What do you want to ask or share?"/>
            <button className="add-comment add-comment1">Post</button>
        </div>      
        <div className="col-md-3 justify-content-end d-flex">
        <button className="new-community  new-community1 d-flex gap-1" onClick={handleSubmit}>
          <AddOutlinedIcon className="zoom-icon" />
          <span>Create Community</span>
        </button>
        </div>  

      </div>
      <Offcanvas
        show={showOffcanvas}
        onHide={handleCloseOffcanvas}
        placement="bottom"
        className="center-offcanvas"
      >
      <Offcanvas.Body className='body-offcanvas'>
          {/* <h5 className='text-center pt-3'>Email verification</h5> */}
          <form className="p-3">
            <div className="form-group mt-3 d-flex flex-column ">
              <label>Create Community</label>
              <span style={{color:'#7a7a7a', fontSize:12}}>Share your interest, host discussions, and more</span>
            </div>
            <div className="form-group mt-3 ">
              <label>Community Name</label>
              <input type="text" placeholder="Enter community name" />
            </div>
            <div className="form-group mt-2 pb-3 d-flex flex-column">
              <label>Brief Description</label>
              <span style={{color:'#7a7a7a', fontSize:12}}>Include a few keywords to show people what to expect if they join.</span>
              <textarea className="mt-3" style={{borderRadius:5, padding:10, minHeight:120}} placeholder="Enter description"></textarea>
            </div>
            {/* <Link to='/login'> */}

            <button type="submit">Create</button>
            {/* </Link> */}
          </form>
        </Offcanvas.Body>
      </Offcanvas>
      <div className="d-flex flex gap-2 content-user">
        <div className="col-md-9 border rounded p-3">
          <div className="img-text-container d-flex flex-row gap-3">
              <img className='commumity-img' src={logo} alt='' />
            <div className="d-flex flex-column">
              <span className="title">Blockchain Society</span>
              <span className="posted-by">Posted By Tutor Sonam Tshering </span>
            </div>
          </div>
          <hr/>
          <div>
            <span className="post-description">
              How do you explain Blockchain technology to someone who 
              doesn't know it?
            </span>
          </div>
          <div className="d-flex -row align-items-center gap-3 justify-content-end">
            <a className="d-flex justify-content-end" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                <ChatBubbleOutlineOutlinedIcon style={{color:'#505050'}}/>
            </a>
            <span className='replies'>4 Replies</span>
          </div>
            <div className="collapse mt-2" id="collapseExample">
              <div className="card-body comment-layout d-flex flex-row gap-2">
                <img className='profile-img' src={logo} alt='' />
                <input type='text' className="border input-comment"/>
                <button className="add-comment">Add Comment</button>
              </div>
              <div className="row comment-section mt-3">
                <div className='profile-others gap-3 d-flex align-items-center'>
                  <img className='profile-img' src={logo} alt='' />
                  <span style={{fontFamily:'bold'}}>Pema Wangmo</span>
                  <span>3 hrs ago</span>
                </div>
                <div className="d-flex flex-row gap-5 justify-content-between">
                  <div className="comments justify-content-end d-flex">
                  </div>
                  <div className="comments">
                    <p>
                      Blockchain is like a digital ledger shared among computers. Transactions are stored in blocks, forming a chain. It's decentralized, so no single entity controls it. Once recorded, transactions are virtually tamper-proof. It's transparent, allowing everyone to see the history. Consensus mechanisms ensure new transactions are valid. Blockchain goes beyond cryptocurrencies, finding uses in supply chains, voting, and contracts. It's about trust without relying on central authorities.
                    </p>
                    <div className="d-flex flex-row gap-2">
                    <button className="vote gap-1 d-flex align-items-center justify-content-center">
                      <i>
                      <ArrowCircleUpOutlinedIcon/>
                      </i>
                      <span>3</span>
                      <span>Votes</span>
                    </button>
                    <button className="reply">Reply</button>
                    <button className="reply">3 <span>Replies</span></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <div className="col-md-3  ">
            <div className='border p-3 rounded'>
              <h6>Top Users</h6>
              <ul className="list-unstyled"> 
                {topUsers.map((user, index) => (
                  <li key={index}>{user}</li>
                ))}
              </ul>
            </div>
          </div>
        
      </div>
    </div>
  );
}

export default StaffDashboard;




  