import React, { useState } from 'react';
import CheckOutlinedIcon from '@mui/icons-material/CheckOutlined';
import Offcanvas from 'react-bootstrap/Offcanvas';

const Community = () => {
  const [joined, setJoined] = useState(false);
  const [imageUrl, setImageUrl] = useState(null);
  const [showOffcanvas, setShowOffcanvas] = useState(false); // State to manage the visibility of the offcanvas

  const handleJoin = () => {
    setJoined(true);
  };

  const handleSubmit = () => {
    setShowOffcanvas(true); // Show the Offcanvas when the "Edit" button is clicked
  };

  const handleCloseOffcanvas = () => {
    setShowOffcanvas(false);
  };

  const handleImageUpload = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      setImageUrl(reader.result);
    };
    reader.readAsDataURL(file);
  };

  return (
    <div className='row community'>
      <div className="justify-content-between mt-3 align-items-center">
        <div className="course-container1 p-3">
          <label className="image-course1 " htmlFor="imageUpload" style={{ cursor: 'pointer' }}>
            <input
            className=""
              id="imageUpload"
              type="file"
              accept="image/*"
              onChange={handleImageUpload}
              style={{ display: 'none' }}
            />
              {imageUrl ? (
                <img className="image-course1    p-3" src={imageUrl} alt="Uploaded Image" />
              ) : (
                  <p className="button-upload text-center">Upload Image</p>
            )}
          </label>
          <div className="community-edit  rounded d-flex flex-row justify-content-between align-items-center">
            <div className="d-flex flex-column ">
              <h5>Programming for blockchain 1</h5>
              <span>Created by Tutor Lhamo</span>
              <span>10 <span>members</span></span>
            </div>
            <div>
              {joined ? (
                <CheckOutlinedIcon color="primary" />
              ) : (
                <button className="member-join" onClick={handleSubmit}>Edit</button> 
              )}
            </div>
          </div>
        </div>
      </div>
      <div >
      <Offcanvas
        show={showOffcanvas}
        onHide={handleCloseOffcanvas}
        placement="bottom"
        className="center-offcanvas"
      >
      <Offcanvas.Body className='body-offcanvas'>
          {/* <h5 className='text-center pt-3'>Email verification</h5> */}
          <form className="p-3">
            <div className="form-group mt-3 d-flex flex-column ">
              <label>Edit Community</label>
              <span style={{color:'#7a7a7a', fontSize:12}}>Share your interest, host discussions, and more</span>
            </div>
            <div className="form-group mt-3 ">
              <label>Community Name</label>
              <input type="text" placeholder="Enter community name" />
            </div>
            <div className="form-group mt-2 pb-3 d-flex flex-column">
              <label>Brief Description</label>
              <span style={{color:'#7a7a7a', fontSize:12}}>Include a few keywords to show people what to expect if they join.</span>
              <textarea className="mt-3" style={{borderRadius:5, padding:10, minHeight:120}} placeholder="Enter description"></textarea>
            </div>
            {/* <Link to='/login'> */}

            <button type="submit">Update</button>
            {/* </Link> */}
          </form>
        </Offcanvas.Body>
      </Offcanvas>
</div>
    </div>
  );
};

export default Community;
