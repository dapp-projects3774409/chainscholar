import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './main.css'
import AddOutlinedIcon from '@mui/icons-material/KeyboardDoubleArrowRightOutlined';
import logo from '../../../images/logo.png';
import ChatBubbleOutlineOutlinedIcon from '@mui/icons-material/ChatBubbleOutlineOutlined';
import ArrowCircleUpOutlinedIcon from '@mui/icons-material/ArrowCircleUpOutlined';
function Dashboard() {
  const topUsers = ['Pema Wangmo', 'Rada Dorji', 'Sapuna Mongar'];


  return (
    <div className='row'>
      <div className="d-flex flex-row justify-content-between mt-3 align-items-center">
        <h5>Dashboard</h5>
        <button className="new-community d-flex gap-1">
        <AddOutlinedIcon className="zoom-icon" />
          <span>Join Community</span>
        </button>
      </div>
      
      <div className="d-flex flex gap-2 content-user">
        <div className="col-md-9 border rounded p-3">
          <div className="img-text-container d-flex flex-row gap-3">
              <img className='commumity-img' src={logo} alt='' />
            <div className="d-flex flex-column">
              <span className="title">Blockchain Society</span>
              <span className="posted-by">Posted By Tutor Sonam Tshering </span>
            </div>
          </div>
          <hr/>
          <div>
            <span className="post-description">
              How do you explain Blockchain technology to someone who 
              doesn't know it?
            </span>
          </div>
          <div className="d-flex -row align-items-center gap-3 justify-content-end">
            <a className="d-flex justify-content-end" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                <ChatBubbleOutlineOutlinedIcon style={{color:'#505050'}}/>
            </a>
            <span className='replies'>4 Replies</span>
          </div>
            <div className="collapse mt-2" id="collapseExample">
              <div className="card-body comment-layout d-flex flex-row gap-2">
                <img className='profile-img' src={logo} alt='' />
                <input type='text' className="border input-comment"/>
                <button className="add-comment">Add Comment</button>
              </div>
              <div className="row comment-section mt-3">
                <div className='profile-others gap-3 d-flex align-items-center'>
                  <img className='profile-img' src={logo} alt='' />
                  <span style={{fontFamily:'bold'}}>Pema Wangmo</span>
                  <span>3 hrs ago</span>
                </div>
                <div className="d-flex flex-row gap-5 justify-content-between">
                  <div className="comments justify-content-end d-flex">
                  </div>
                  <div className="comments">
                    <p>
                      Blockchain is like a digital ledger shared among computers. Transactions are stored in blocks, forming a chain. It's decentralized, so no single entity controls it. Once recorded, transactions are virtually tamper-proof. It's transparent, allowing everyone to see the history. Consensus mechanisms ensure new transactions are valid. Blockchain goes beyond cryptocurrencies, finding uses in supply chains, voting, and contracts. It's about trust without relying on central authorities.
                    </p>
                    <div className="d-flex flex-row gap-2">
                    <button className="vote gap-1 d-flex align-items-center justify-content-center">
                      <i>
                      <ArrowCircleUpOutlinedIcon/>
                      </i>
                      <span>3</span>
                      <span>Votes</span>
                    </button>
                    <button className="reply">Reply</button>
                    <button className="reply">3 <span>Replies</span></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <div className="col-md-3  ">
            <div className='border p-3 rounded'>
              <h6>Top Users</h6>
              <ul className="list-unstyled"> 
                {topUsers.map((user, index) => (
                  <li key={index}>{user}</li>
                ))}
              </ul>
            </div>
          </div>
        
      </div>
    </div>
  );
}

export default Dashboard;




  