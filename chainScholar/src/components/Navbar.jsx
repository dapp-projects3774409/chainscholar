import React, {useState}from 'react';
import logo from '../images/logo.png';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';


function Navbar(){
    const [nav, setnav] = useState (false);
    const changeBackground = () =>{
        if (window.scrollY >= 50){
            setnav(true);
        }else{
            setnav(false);
        }
}
window.addEventListener('scroll',changeBackground);
    return (
        <nav className ={nav ? 'nav active' : 'nav'}>
            <a href = '.' className='logo align-items-center col-md-4'>
                <img src={logo} alt = ''/>
            </a>
            <ul className = 'menu gap-3 col-md-8 justify-content-end'>
                   {/* <div class=" d-flex flex-row gap-3 text-decoration-none align-items-center"> */}
                     <li>
                        {/* <Link to="/register" style={{textDecoration:"none"}}> */}
                            <a href='/' className = 'text-decoration-none p-0'>Home</a>
                        {/* </Link> */}
                        </li>                    
                        <li>
                            {/* <Link to="/register" style={{textDecoration:"none"}}> */}
                                <a href='/' className = 'text-decoration-none p-0'>About</a>
                            {/* </Link> */}
                        </li>
                        <li>
                            {/* <Link to="/register" style={{textDecoration:"none"}}> */}
                            <a href='/' className = 'text-decoration-none p-0'>Mission</a>
                            {/* </Link> */}
                        </li>
                   {/* </div> */}
                   {/* <div class="col-md-4 d-flex flex-row align-items-center"> */}
                    <li>
                        <Link to="/sign-up"  style={{textDecoration:"none"}}>
                            <button className="btn-sign p-0">Sign Up</button>
                        </Link>  
                    </li>
                    {/* </div> */}
                    
                </ul>
        </nav>
    )
}

export default Navbar;