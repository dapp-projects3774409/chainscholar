import "../styles/Signup.css";
import "../styles/LandingPage.css";
import logo from "../img/logo.png";
import staff from "../img/picture4.png";
import student from "../img/picture5.png";
import React, { useState } from "react";

function Login() {
  const [accountType, setAccountType] = useState("");

  const handleAccountTypeClick = (type) => {
    setAccountType(type);
  };

  return (
    <div className="container">
      <header>
        <div className="logo">
          <img
            src={logo}
            alt="logo"
            style={{ width: "110px", height: "80px", marginLeft: "15px" }}
          />
        </div>
        <nav>
          <a href="#main-content">Home</a>
          <a href="#content2">About</a>
          <a href="#text-content">Mission</a>
          <button className="signup-button">
            <span>Signup</span>
          </button>
        </nav>
      </header>
      <div className="signup-container">
        <h2 style={{ fontSize: "32px", marginTop: "-4px" }}>Login</h2>
        <p style={{ fontSize: "20px" }}>Choose Account Type</p>
        <div className="account-type-options">
          <button
            className={`account-button ${
              accountType === "staff" ? "active" : ""
            }`}
            onClick={() => handleAccountTypeClick("staff")}
          >
            <img src={staff} alt="Staff" />
            Staff
          </button>
          <button
            className={`account-button ${
              accountType === "student" ? "active" : ""
            }`}
            onClick={() => handleAccountTypeClick("student")}
          >
            <img src={student} alt="Student" />
            Student
          </button>
        </div>

        {/* Form Inputs */}

        <input type="email" placeholder="Email" />
        <input type="password" placeholder="Password" />

        <div className="signupButton">
          <p>
            No account?{" "}
            <a href="/login" style={{ color: "#7E22CE" }}>
              Signup
            </a>
            <button type="submit">Login</button>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Login;
