import React, { useState } from "react";
import "../styles/exchangeRecord.css";

function CommunityStatus() {
  const [gPoints, setGPoints] = useState("");
  const [gCoins, setGCoins] = useState("");
  return (
    <div className="earning-status">
      <div className="exchange-content">
        <div className="coinLabels">
          <div
            style={{
              marginTop: "30px",
              marginBottom: "10px",
              marginLeft: "30px",
            }}
          >
            <label
              htmlFor="G-points"
              style={{
                // background: "#7e22ce",
                // padding: "6px",
                color: "Black",
                borderRadius: "2px",
                fontWeight: "bold",
              }}
            >
              Community Name
            </label>
            <input
              type="number"
              value={gPoints}
              onChange={(e) => setGPoints(e.target.value)}
              style={{ marginTop: "8px" }}
            />
          </div>

          <div
            style={{
              marginTop: "10px",
              marginRight: "20px",
              marginLeft: "30px",
            }}
          >
            <label
              htmlFor="G-coins"
              style={{
                // background: "#7e22ce",
                // padding: "6px",
                color: "black",
                borderRadius: "2px",
                fontWeight: "bold",
              }}
            >
              Community Description
            </label>
            <input
              type="number"
              readOnly
              value={gCoins}
              onChange={(e) => setGCoins(e.target.value)}
              style={{ marginTop: "8px" }}
            />
          </div>
        </div>

        <button
          style={{
            backgroundColor: "#7e22ce",
            color: "white",
            border: "none",
            // padding: "8px 16px",
            borderRadius: "4px",
            cursor: "pointer",
            marginLeft: "50px",
            marginTop: "-10px",
          }}
        >
          Update
        </button>
      </div>
    </div>
  );
}

export default CommunityStatus;
