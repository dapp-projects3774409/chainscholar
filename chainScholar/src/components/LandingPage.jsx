import "../styles/LandingPage.css";
import logo from "../img/logo.png";
import homePicture from "../img/picture1.png";
import coinLogo from "../img/coinLogo.png";
import block from "../img/picture3.png";
import customPicture from "../img/picture2.png";
import Address from "./Address";
import Policy from "./Policy";
import SocialMedia from "./SocialMedia";
import QuickLink from "./QuickLink";
import "@fortawesome/fontawesome-free/css/all.min.css";

const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "100vh",
    backgroundColor: "#fff",
  },
  image: {
    width: 325,
    height: 325,
  },
  text1: {
    fontSize: 34,
    fontWeight: "bold",
    marginTop: -10,
    marginBottom: 10,
  },
  text2: {
    fontSize: 34,
    fontWeight: "bold",
    marginTop: -8,
    marginBottom: 20,
  },
  button: {
    backgroundColor: "#8A2BE2",
    color: "#fff",
    padding: "10px 20px",
    border: "none",
    borderRadius: 5,
    cursor: "pointer",
    width: "120px",
    height: "40px",
    fontSize: 18,
  },
};

function LandingPage() {
  return (
    <div className="container">
      <header>
        <div className="logo">
          <img
            src={logo}
            alt="logo"
            style={{ width: "110px", height: "80px", marginLeft: "15px" }}
          />
        </div>
        <nav>
          <a href="#main-content">Home</a>
          <a href="#content2">About</a>
          <a href="#text-content">Mission</a>
          <button className="signup-button">
            <span>Signup</span>
          </button>
        </nav>
      </header>
      <div className="content">
        <main className="main-content" id="main-content">
          <div>
            <h1
              style={{
                fontSize: "60px",
                marginBottom: "5px",
                marginTop: "50px",
              }}
            >
              ChainScholars
            </h1>
            <p style={{ fontSize: "30px", marginBottom: "4px" }}>
              Platform that rewards active participation and
            </p>
            <p style={{ fontSize: "30px" }}>
              fosters transparent knowledge exchange
            </p>
          </div>
          <button className="learn-more-button">
            <a href="#illustration">
              <span style={{ color: "white" }}>Learn More</span>
            </a>
          </button>
        </main>
        <section className="illustration">
          <img
            src={homePicture}
            alt="homePicture"
            style={{ width: "560px", height: "417px", marginLeft: "15px" }}
          />
        </section>
      </div>
      <div style={styles.container}>
        <img
          id="illustration"
          src={coinLogo}
          alt="G-Coin"
          style={styles.image}
        />
        <p style={styles.text1}>
          <span style={{ fontSize: "40px" }}>G-coin</span> powering the
          ChainScholars{" "}
        </p>
        <p style={styles.text2}>ecosystem</p>

        <button style={styles.button}>
          {" "}
          <a href="#content2" style={{ textDecoration: "none" }}>
            <span style={{ color: "white" }}>Explore</span>
          </a>
        </button>
      </div>
      <div className="header">
        <div className="image-container">
          <img src={block} alt="ChainScholars" />
        </div>
        <div className="content2" id="content2">
          <h1>Decentralized, Incentivized, Transparent</h1>
          <p>
            ChainScholars is a decentralized platform that incentivizes active
            participation through rewards, fostering collaboration and
            recognition, while ensuring transparency and immutability through
            blockchain technology.
          </p>
        </div>
      </div>
      <div className="container1">
        <div className="text-content" id="text-content">
          <h2>Why Custom Reward Mechanism?</h2>
          <p style={{ fontSize: "1.5em", marginTop: "-22px" }}>
            A custom reward mechanism within ChainScholars encourages engagement
            and contributions, ensuring users are recognized and incentivized
            for their valuable input and collaboration within the academic
            community.
          </p>
        </div>
        <div className="image-content1" style={{ marginBottom: "280px" }}>
          <img src={customPicture} alt="lastpicture" />
        </div>
      </div>
      <footer className="footer">
        <div className="container2">
          <Address />
          <Policy />
          <QuickLink />
        </div>
        <div className="line"></div>
        <div className="copyright-social">
          <img
            src={logo}
            alt="logo"
            style={{ width: "110px", height: "80px", marginLeft: "15px" }}
          />
          <p className="copyright" style={{ fontSize: "11px" }}>
            Copyright@ ChainScholars 2024. All Rights Reserved.
          </p>
          <SocialMedia />
        </div>
      </footer>
    </div>
  );
}

export default LandingPage;
