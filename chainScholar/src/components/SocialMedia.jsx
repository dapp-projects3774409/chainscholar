import React from "react";

function SocialMedia() {
  return (
    <div className="social-media">
      <a href="#">
        <i className="fab fa-facebook"></i>
      </a>
      <a href="#">
        <i className="fab fa-instagram"></i>
      </a>
      <a href="#">
        <i className="fab fa-twitter"></i>
      </a>
    </div>
  );
}

export default SocialMedia;
