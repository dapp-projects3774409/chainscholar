import React from 'react'
import Header from '../Header';
import Features from '../Features'
// import Main from './Main';
// import { Routes,Route } from 'react-router-dom';
// import Dashboard from './Components/Content/Main/Dashboard';
// import Explore from './Components/Content/Main/Explore';
function LandingLayout() {
  return (
    <div className="App">
      <Header/>
      <Features/>
    </div>  )
}

export default LandingLayout