import React from "react";
import CommunityList from "./StaffcommunityList";
import EarningStatus from "./communityUpdate";
import logo from "../img/logo.png";
import { useState } from "react";
import "../styles/userProfile.css";
import student from "../img/picture5.png";
import upload from "../img/upload.png";

function StaffProfile() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [file, setFile] = useState(null);

  // const handleNameChange = (e) => {
  //   setName(e.target.value);
  // };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    setFile(file);
  };

  // const handleSubmit = (e) => {
  //   e.preventDefault();
  //   // Perform form submission logic here
  //   console.log("Name:", name);
  //   console.log("Email:", email);
  //   console.log("File:", file);
  //   // Reset form fields
  //   setName("");
  //   setEmail("");
  //   setFile(null);
  // };

  // // popup exchange
  // const [renameModalIsOpen, setRenameModalIsOpen] = useState(false);
  // const [newName, setNewName] = useState("");

  // const openRenameModal = () => {
  //   setRenameModalIsOpen(true);
  // };

  // const closeRenameModal = () => {
  //   setRenameModalIsOpen(false);
  //   setNewName("");
  // };

  const handleNameChange = (e) => {
    setNewName(e.target.value);
  };

  const handleSubmit = () => {
    // Handle the renaming logic here
    console.log("New name:", newName);
    closeRenameModal();
  };

  // const [deleteModalIsOpen, setDeleteModalIsOpen] = useState(false);

  // const openDeleteModal = () => {
  //   setDeleteModalIsOpen(true);
  // };

  // const closeDeleteModal = () => {
  //   setDeleteModalIsOpen(false);
  // };

  // const handleDelete = () => {
  //   // Implement delete account logic here
  //   console.log("Account deleted");
  //   closeDeleteModal();
  // };
  return (
    <div className="profile-container">
      <div className="profileContent">
        <div className="profile-card">
          <img src={student} alt="Profile Picture" />
          <h2>Lhamo</h2>
          <p>Lhamo@gmail.com</p>
          <p>Account: 0xasdfgh1fsdvd</p>
          {/* Add form for updating profile information */}
        </div>
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="name">Name:</label>
            <input
              type="text"
              id="name"
              value={name}
              onChange={handleNameChange}
              required
            />
          </div>
          <div>
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              id="email"
              value={email}
              onChange={handleEmailChange}
              required
            />
          </div>
          <div className="file-input-container">
            <label htmlFor="file" className="file-input-label">
              Picture:
              <img
                src={upload}
                alt="upload"
                style={{ width: "25px", height: "25px", marginRight: "85px" }}
              />
            </label>
            <input
              type="file"
              id="file"
              accept=".jpg, .jpeg, .png, .pdf"
              onChange={handleFileChange}
              required
            />
          </div>

          <button
            type="submit"
            className="updateButton"
            style={{
              backgroundColor: "#7e22ce",
              width: "85px",
              height: "32px",
              alignSelf: "center",
            }}
          >
            Update
          </button>
        </form>
      </div>

      <div className="info-section">
        <CommunityList />
        <EarningStatus />
        <img
          src={logo}
          alt="logo"
          style={{
            width: "230px",
            height: "170px",
            marginLeft: "60px",
          }}
        />
      </div>
    </div>
  );
}

export default StaffProfile;
