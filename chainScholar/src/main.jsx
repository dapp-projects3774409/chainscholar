import React, { useState } from "react";
import MySidebar from "../src/Components/Content/Sidebar";
import Dashboard from "../src/Components/Content/Main/Dashboard";
import Footer from "../src/Components/Content/Footer";
import "./index.css";
import logo from "./images/logo.png";
import Explore from "./Components/Content/Main/Explore";
// import OtpEnter from "./Components/Registration/VerifyOtp";
import Login from "./Components/Login/Login";

import {
  BrowserRouter,
  Routes,
  Route,
  Router,
  Switch,
  Navigate,
} from "react-router-dom";
import About from "./Components/Content/Main/About";
import CommunityDetails from "./Components/Content/Main/CommunityDetails";
import CreateCommunity from "./Components/Content/Main/CreateCommunity";
import StaffDashboard from "./Components/Content/Main/StaffDashboard";
// import Profile from "./Components/Content/Main/userProfile";
import StaffProfile from "./Components/Content/Main/staffProfile";

import { Link } from "react-router-dom";
import Registeration from "./Components/Registration/Registration";

function Main() {
  const [isSidebarOpen, setIsSidebarOpen] = useState(true);
  const [currentContent, setCurrentContent] = useState("Dashboard");

  const toggleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };

  const handleContentChange = (content) => {
    setCurrentContent(content);
  };

  return (
    <>
      <div className="app-container" style={{ display: "flex" }}>
        <div className="sidebar-container">
          <MySidebar
            setCurrentContent={handleContentChange}
            toggleSidebar={toggleSidebar}
          />
        </div>

        <div className="main-content" style={{ flex: 1, overflow: "hidden" }}>
          <div className="header-content justify-content-end d-flex gap-3">
            <span style={{ fontFamily: "medium" }}>Logout</span>
          </div>
          <div
            className="container-body"
            style={{ maxWidth: "100%", overflow: "hidden" }}
          >
            <Routes>
              <Route path="/dashboard" element={<Dashboard />} />
              <Route path="/staff-dashboard" element={<StaffDashboard />} />
              <Route path="/explore/*" element={<Explore />} />
              <Route
                path="/explore/community-details"
                element={<CommunityDetails />}
              />
              <Route path="/about" element={<About />} />
              <Route path="/community" element={<CreateCommunity />} />
              <Route path="/profile" element={<StaffProfile />} />
              {/* <Route path="/profile" element={<Profile />} /> */}

              <Route path="/registeration" element={<Registeration />} />
              <Route path="/login" element={<Login />} />

              {/* <Route path="/otpenter" element={<OtpEnter />} /> */}
            </Routes>
          </div>
        </div>
      </div>
      <div>
        <Footer />
      </div>
    </>
  );
}

export default Main;
