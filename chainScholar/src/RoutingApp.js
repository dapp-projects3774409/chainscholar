
import React from 'react';
import './index.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'; // Use Routes instead of Route
import RegistrationForm from './Components/Registration/Registration';
import CompanyDetails from './Components/Registration/CompanyDetail';
import LOGIN from '../src/Components/Login/Login'
import LandingLayout from './Components/LandingLayout/landingLayout';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import Main from './Main';

  export const RoutingApp = () => {
    return (
      <div>
      <Routes>
        <Route path="/" element={<LandingLayout />} /> 
        <Route path="/sign-up" element={<RegistrationForm />} /> 
        <Route path="/sign-up-details" element={<CompanyDetails />} />
        <Route path="/login" element={<LOGIN />} />
        <Route path="/*" element={<Main />} />
        </Routes>
      </div>
    );
  };
  