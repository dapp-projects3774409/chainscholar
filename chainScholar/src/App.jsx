import LandingPage from "./components/LandingPage";
import Signup from "./components/Signup";
import Login from "./components/Login";
import ScrollToTopArrow from "./components/ScrollingArrow";
import UserProfile from "./components/userProfile";
import StaffProfile from "./components/StaffProfile";

function App() {
  return (
    <div className="App">
      {" "}
      <LandingPage />
      <Signup />
      <Login />
      <ScrollToTopArrow />
      <StaffProfile />
      <Exchangerecord />
    </div>
  );
}

export default App;
